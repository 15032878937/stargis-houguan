import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "../components/layout/layout.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Index",
    component: Layout,
    meta: {
      title: "首页",
      requiresAuth: true,
    },
    children: [
      {
        path: "",
        name: "Home",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/home/index.vue"),
        meta: {
          title: "首页",
          requiresAuth: true

        }
      },
      {
        path: "/data-plaza",
        name: "DataPlaza",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/data-plaza/index.vue"),
        meta: {
          title: "数据广场",
          requiresAuth: true

        }
      },
      {
        path: "/service-module",
        name: "ServiceModule",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/service-module/index.vue"),
        meta: {
          title: "服务模块",
          requiresAuth: true

        }
      },
      {
        path: "/scene-components",
        name: "SceneComponents",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/scene-components/index.vue"),
        meta: {
          title: "场景组件",
          requiresAuth: true

        }
      },
      {
        path: "/data-source",
        name: "DataSource",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/data-source/index.vue"),
        meta: {
          title: "数据源模块",
          requiresAuth: true
        }
      },
      {
        path: "/organization",
        name: "Organization",
        meta: {
          title: "组织机构管理",
          requiresAuth: true
        },
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/organization/index.vue"),
        children: [
          {
            path: "/user-manage",
            name: "UserManage",
            component: () =>
              import(/* webpackChunkName: "about" */ "../views/organization/user-manage/user-manage.vue"),
            meta: {
              title: "用户管理",
              requiresAuth: true
            }
          },
          {
            path: "/role-anage",
            name: "RoleAnage",
            component: () =>
              import(/* webpackChunkName: "about" */ "../views/organization/role-anage/role-manage.vue"),
            meta: {
              title: "角色管理",
              requiresAuth: true
            }
          },
          {
            path: "/institution",
            name: "Institution",
            component: () =>
              import(/* webpackChunkName: "about" */ "../views/organization/institution/institution.vue"),
            meta: {
              title: "机构管理",
              requiresAuth: true
            }
          },

        ]
      },
      {
        path: "/share-manage",
        name: "ShareManage",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/share-manage/index.vue"),
        meta: {
          title: "共享管理",
          requiresAuth: true
        }
      },
      {
        path: "/develop-guide",
        name: "DevelopGuide",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/develop-guide/index.vue"),
        meta: {
          title: "开发指南",
          requiresAuth: true
        }
      },
      {
        path: "/platform-run",
        name: "PlatformRun",
        component: () =>
          import(/* webpackChunkName: "about" */ "@/views/platform-run/index.vue"),
        meta: {
          title: "平台运行",
          requiresAuth: true
        }
      },
    ],
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/login/index.vue"),
  },
  {
    path: "*",
    name: "Error",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/error/404.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// 守卫
// 访问的白名单, 可以直接通行
router.beforeEach((to, from, next) => {
  console.log(to.fullPath);
  const token = JSON.parse(localStorage.getItem('user'))?.token
  if (to.meta.requiresAuth && !token) {
    next({
      path: "/login",
      query: {
        redirect: to.fullPath
      }
    })
  }
  next()
})
export default router;



